// replace these values with those generated in your TokBox Account
var apiKey = "45828062";
var sessionId = "1_MX40NTgyODA2Mn5-MTU4ODE3Mzg2NDM0M35OSzdFSkN0K3lmb2UxZzJFSEdoekRNcTh-UH4";
var token = "T1==cGFydG5lcl9pZD00NTgyODA2MiZzaWc9YWFiZTk1NjNkNjk2Mzk1MjU2ODhjM2ZkN2ExNWY3ZjMzYWVkNWFmYjpzZXNzaW9uX2lkPTFfTVg0ME5UZ3lPREEyTW41LU1UVTRPREUzTXpnMk5ETTBNMzVPU3pkRlNrTjBLM2xtYjJVeFp6SkZTRWRvZWtSTmNUaC1VSDQmY3JlYXRlX3RpbWU9MTU4ODE3Mzg2OSZub25jZT0wLjk3ODc3MTkzNzA4NDk2Mzcmcm9sZT1wdWJsaXNoZXImZXhwaXJlX3RpbWU9MTU4ODI2MDI2OQ==";

// (optional) add server code here
initializeSession();

// Handling all of our errors here by alerting them
function handleError(error) {
  if (error) {
    alert(error.message);
  }
}

function initializeSession() {
  var session = OT.initSession(apiKey, sessionId);

  // Subscribe to a newly created stream
  session.on('streamCreated', function(event) {
  session.subscribe(event.stream, 'subscriber', {
    insertMode: 'append',
    width: '100%',
    height: '100%'
  }, handleError);
});

  // Create a publisher
  var publisher = OT.initPublisher('publisher', {
    insertMode: 'append',
    width: '100%',
    height: '100%'
  }, handleError);

  // Connect to the session
  session.connect(token, function(error) {
    // If the connection is successful, publish to the session
    if (error) {
      handleError(error);
    } else {
      session.publish(publisher, handleError);
    }
  });
}