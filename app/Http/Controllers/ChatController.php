<?php namespace App\Http\Controllers;
use DB;

class ChatController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */

	 public function chat()
    {   
		$appointments=DB::table('appointment')->orderBy('createAt', 'desc')->paginate('2');
		return view('call', ['appointments'=>$appointments]);
		// echo "what happened?";
    }

}
