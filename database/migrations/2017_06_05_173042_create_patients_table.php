<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('patients', function(Blueprint $table)
		{
			$table->increments('id');
			$table->int('userid');
			$table->string('Name');
			$table->integer('Age');
			$table->string('Mobile');
			$table->string('Landline');
			$table->string('Address');
			$table->string('email');
			$table->string('Sex');
			$table->text('Notes');
			$table->string('Photo');
			$table->string('Files');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('patients');
	}

}
