<?php $__env->startSection('title'); ?>
    <title>Telemed | Call Patient</title>
<?php $__env->stopSection(); ?>

<head>
    
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">
  
    <meta name="description" content="Audio+Video+Screen Sharing using RTCMultiConnection" />
    <meta name="keywords" content="WebRTC,RTCMultiConnection,Demos,Experiments,Samples,Examples" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
        * {
            word-wrap:break-word;
        }
        video {
            object-fit: fill;
            width: 30%;
        }
        button,
        input,
        select {
            font-weight: normal;
            padding: 2px 4px;
            text-decoration: none;
            display: inline-block;
            text-shadow: none;
            font-size: 16px;
            outline: none;
        }
        .make-center {
            text-align: center;
            padding: 5px 10px;
        }
        img, input, textarea {
          max-width: 100%
        }
        @media  all and (max-width: 500px) {
            .fork-left, .fork-right, .github-stargazers {
                display: none;
            }
        }
        input{
            margin: auto;
            width: 50% ;
            text-align: center;
        }
        .chat-output{
            font-family: arial;
            font-size:18px;
            text-align: left;
        }
    </style>
</head>


<script type="text/javascript">
/*Script for download Chat andd page */
    function download(){
    var a = document.body.appendChild(
        document.createElement("a")
    );
    a.download = "export.html";
    a.href = "data:text/html," + document.getElementById("chat-container").innerHTML; // Grab the HTML
    a.click(); // Trigger a click on the element
}
</script>


<script type="text/javascript">
/*Get Room id in send notification Section */
  function getlink(){var bla = window.location.href+'?roomid='+$('#room-id').val();
  $('#room-id1').val(bla);
  $('#room-id2').val(bla);  
}
</script>


<script type="text/javascript">
   

    function getpatientdata(){
    if ($("#Selectpatient").val() != ''){   
        $.ajax({
           type:'post',
            url:'ajaxgetpatient',
            data: {'patientid':$("#Selectpatient").val(), '_token': $('input[name=_token]').val()},
            dataType:'html',

            success:function(response){
                var filesselector = '<select  name="patientfilesselector" class="form-control" id="ShareSelect"> '+response+'</select><br>';
               $("#patientfiles").html(filesselector);
               getcallid();
            }                
        })
        }
        else {
            $("#patientfiles").html('')
        }
    }



    function getcallid(){
    if ($("#Selectpatient").val() != ''){   
        $.ajax({
           type:'post',
            url:'ajaxgetcallid',
            data: {'patientid':$("#Selectpatient").val(), '_token': $('input[name=_token]').val()},
            dataType:'html',

            success:function(response){
               $("#sendNotModel").html(response);
               $("#sendNot").prop('disabled', false);
               document.getElementById('sharebtn').disabled = false;
            }  
               })
            }
    else {
            $("#sendNotModel").html('')
            $("#sendNot").prop('disabled',true);

        }

        }

</script>

<?php $__env->startSection('menu'); ?>
<?php if(Auth::user()): ?>
<li><a href="home" >HOME</a></li>
<li ><a href="viewpatient">PATIENTS</a></li>
<li class="active"><a href="callpatient">CALL</a></li>
<li ><a href="setup">SETUP</a></li>
<?php endif; ?>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
<div id="patientfiles1"></div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <ul class="list-group text-right">
                <li class="list-group-item "><a href="#" class="left-nav-active">Make Call</a></li>
              
                
            </ul>    
            
            <div style="padding: 10px">
                <h4 class="text text-success">Appointments</h4>
                <div id="appointment">
                    <?php $__currentLoopData = $appointments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $appointment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="card" style="width: 100%;">
                        <hr>
                        <div class="card-body">
                          <h4 class="card-title"><?php echo e($appointment->name); ?></h4>
                          <h5 class="card-subtitle mb-2 text-muted"><span class="text text-primary">Email:</span> <?php echo e($appointment->email); ?></h5>
                          <h5 class="card-subtitle mb-2 text-muted"><span class="text text-primary">Phone:</span> <?php echo e($appointment->phone); ?></h5>
                          <h5 class="card-subtitle mb-2 text-muted"><span class="text text-primary">DateTime:</span> <?php echo e($appointment->datetimeAppointment); ?> </h5>
                          <p class="card-text"><?php echo e($appointment->message); ?></p>
                          <div>
                            <input type="hidden" value="<?php echo e($appointment->patientProfileId); ?>">
                            <button class="slt-for-call btn btn-info">select me</button>
                          </div>
                        </div>
                      </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php echo e($appointments->links()); ?>

                </div>
            </div>

        </div>


 <div class=" col-md-9 text-center" style="overflow-x:auto;">
        <section class="experiment">
            <div class="make-center">

        <?php if(session('status')): ?>
    <div id="alertmsg" class="alert alert-success">
        <?php echo e(session('status')); ?>

    </div>
    <?php endif; ?>
                 
         <div class="row">
                <div class="col-md-3">

                <input class="form-control full-width" type="text" id="room-id"  value="abcdef" autocorrect=off autocapitalize=off >

                 <?php if(Auth::user()): ?>
                 <?php 
                        $patients=DB::table('patientprofile')->get();
                        $patientfiles=DB::table('patientprofile')->get();      
                ?>
                 <form>
                     <br>
                    <select   name="patientid" class="form-control" id="Selectpatient" onchange="getpatientdata();">
                     <option value=''>Select Patient</option>
                        <?php $__currentLoopData = $patients; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $patient): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                  
                            <option value='<?php echo e($patient->PatientProfileId); ?>'><?php echo e($patient->First); ?> <?php echo e($patient->Middle); ?> <?php echo e($patient->Last); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select> 
                 </form>
                    <?php else: ?> 
                    <br>
                    <?php endif; ?>

               
                <button id="open-room"  <?php if(Auth::guest()): ?> style="display: none;" <?php endif; ?> class="btn btn-primary btn-cus container-fluid btnmarg">Start Call</button>
                
                <button id="join-room" class="btn  btn-green container-fluid btnmarg">Join Room</button> 

                <?php if(Auth::user()): ?>
                <button id="sendNot"  class="btn btn-primary btn-cus container-fluid btnmarg" data-toggle="modal" data-target="#sendNotModel">Send Notification</button>
                <br>
                <?php endif; ?>



                <button class="disnone" id="open-or-join-room" class="btn btn-info">Auto Open Or Join Room</button>
                 
                <hr>


                <div id="patientfiles" > <!--Here Will Contain Files Selector--> </div>
                

                <br>

                <?php if(Auth::user()): ?>

                <button id="sharebtn" disabled class="btn btn-primary btn-cus container-fluid btnmarg">Share Selected File</button> 


                <?php endif; ?>
                <button id="share-file" disabled class="btn btn-green container-fluid btnmarg">Upload File</button>
                <button  id="share-screen" disabled class="btn  btn-green container-fluid btnmarg">Share Screen</button> 
                
                 <div id="room-urls"></div> 

            </div>

                <div class="col-md-9">
                        <div id="videos-container" class="videoscontainer">
                        </div> <br>
                        
                        <div class="filesection chat-output"> 
                <!--Here Will Contain Files after submitted to shared and Screen-->  
                <div class="file-container" id="file-container"></div>
                </div> <br>


                            <table>
                                <tbody>
                                    <tr>
                                        <td  width="85%"><input class="form-control" type="text" id="input-text-chat" placeholder="Enter Text Chat" ></td>
                                        <td>&nbsp;&nbsp;&nbsp;</td>
                                        <td>
                                             <?php if(Auth::user()): ?>
                                                <button  class="btn btn-primary btn-cus container-fluid " onclick="return(download())" >Save Notes</button>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            
     
                           
                        </div>
                         <br>
                </div>
            </div>
           
        </section>
     </div>    

</div>
</div>



<?php if(Auth::user() && isset($patient)): ?>
<!-- Modal -->
<div id="sendNotModel" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Send Notification</h4>
      </div>
      <div class="modal-body">
        <p>Email Notification.</p>
        <span> Patient Email : <?php echo e($patient->EMailAddress); ?></span>
                    <form target="_blank" method="post" action="sendemail">
                    <input name="message" id="room-id1" onclick="return getlink();" placeholder="Click to insert chat link" value="">
                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                    <input type="hidden" name="name" value="Patient System">
                    <input type="hidden" name="mail" value="<?php echo e($patient->EMailAddress); ?>">
                    <input type="hidden" name="subject" value="Chat-Request">
                    <button type="submit" class="btn btn-primary btn-cus">Send Email</button>
                    </form>

        <p>SMS Notification.</p>
        <span> Patient Mobile:<?php echo e($patient->Phone1); ?></span>
        <br>
        <span> Patient Phone Carrier :<?php echo e($patient->PhoneCarrier); ?></span>
                    <form target="_blank" method="post" action="sendsms">
                    <input name="message" id="room-id2" onclick="return getlink();" placeholder="Click to insert chat link" value="">
                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                    <input type="hidden" name="name" value="Patient System">
                    <input type="hidden" name="mobile" value="<?php echo e($patient->Phone1); ?>">
                    <input type="hidden" name="PhoneCarrier" value="<?php echo e($patient->PhoneCarrier); ?>">
                    <input type="hidden" name="subject" value="Chat-Request">
                    <button type="submit" class="btn btn-primary btn-cus">Send SMS</button>
                    </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<?php endif; ?>


        <script src="https://rtcmulticonnection.herokuapp.com/dist/RTCMultiConnection.min.js"></script>
        <script src="https://rtcmulticonnection.herokuapp.com/dev/globals.js"></script>
        <script src="https://rtcmulticonnection.herokuapp.com/socket.io/socket.io.js"></script>
        <script src="https://cdn.webrtc-experiment.com:443/FileBufferReader.js"></script>
        <!-- custom layout for HTML5 audio/video elements -->
        <script src="https://cdn.webrtc-experiment.com/getMediaElement.js"></script>
        <!-- capture screen from any HTTPs domain! -->
        <script src="https://cdn.webrtc-experiment.com:443/getScreenId.js"></script>
        <!--File Of Chat-->
        <script src="js/chat.js" ></script>

            <?php if(Auth::user()): ?>
            <script type="text/javascript">
            /* Sharing Patient Files */
              document.getElementById('sharebtn').onclick = function() {    
                         
                var file=document.getElementById('ShareSelect').value;

            /*Check file exxtensions*/
            var extension = file.substr( (file.lastIndexOf('.') +1) );
                switch(extension) {
                    case 'jpg':
                    case 'png':
                    case 'gif':
                        var link ='Me : <img src="'+file+'">' ;
                        connection.send(link);  
                        appendDIV(link);  
                    break;                         
                    case 'zip':
                    case 'rar':
                        var link ='Me : <a href="'+file+'">'+file+'</a>' ;  
                        connection.send(link);  
                        appendDIV(link);                 
                    break;
                    case 'pdf':
                         var link ='<iframe style="resize:both;" src="'+file+'" />'
                        connection.send(link);  
                        appendDIV(link);      
                    break;
                    case 'mpg':
                    case 'MPG':
                    case 'mp4':
                    case 'MP4':
                    case 'ogg':
                    case 'rm':
                    case 'WMV':
                    case 'wmv':
                    case 'AVI':
                    case 'avi':
                    case 'ASF':
                    case 'asf':
                    case 'MOV':
                    case 'mov':
                    case 'FLV':
                    case 'flv':
                    case 'SWF':
                    case 'swf':
                        var link ='Me :<video controls style="width:100%"><source src="'+file+'" type="video/mp4"></video>';
                        connection.send(link);  
                        appendDIV(link);
                    break;
                    case 'txt':
                        jQuery.get(file, function(data) {
                        connection.send(data);  
                        appendDIV(data);      
                        });
                    break;
                   default:
                    var link ='Me : <a href="'+file+'">'+file+'</a>' ;
                    connection.send(link);  
                    appendDIV(link);            
                }

            };  
            </script>
            <?php else: ?> 
            <?php endif; ?> 

            
            
            <?php if(Auth::user()): ?>
            <script type="text/javascript">
            document.getElementById('input-text-chat').onkeyup = function(e) {
                if (e.keyCode != 13) return;
                // removing trailing/leading whitespace
                this.value = this.value.replace(/^\s+|\s+$/g, '');
                if (!this.value.length) return;
                var divSelector=appendDIV;
                connection.send(this.value);
                appendDIV('Me : <b style="color:#19496f">'+this.value+'</b>');
                this.value = '';
            };
            </script>
            <?php else: ?> 
             <script type="text/javascript">
            document.getElementById('input-text-chat').onkeyup = function(e) {
                if (e.keyCode != 13) return;
                // removing trailing/leading whitespace
                this.value = this.value.replace(/^\s+|\s+$/g, '');
                if (!this.value.length) return;
                connection.send(this.value);
                appendDIV('Patient : <b style="color:#19496f">: '+this.value+'</b>');
                this.value = '';
            };
            </script>
            <?php endif; ?>

        <script>
             $("button.slt-for-call").click(function(){
                    $(".slt-for-call").prop('disabled', false);
                    $(this).prop('disabled', true);
                    var patientId = $(this).parent().children('input').val();
                    $("#Selectpatient").val(patientId);
                    getpatientdata();
                });
            window.useThisGithubPath = 'muaz-khan/RTCMultiConnection';
        </script>
        <script src="https://cdn.webrtc-experiment.com/commits.js" async></script>
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.4.3.min.js" ></script>
<?php $__env->stopSection(); ?>




<?php echo $__env->make('layouts.public', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>