

<?php $__env->startSection('title'); ?>
    <title>Telemed | Setup</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('menu'); ?>
<li><a href="home" >HOME</a></li>
<li><a href="viewpatient">PATIENTS</a></li>
<li><a href="callpatient">CALL</a></li>
<li  class="active"><a  href="setup">SETUP</a></li>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
<script type="text/javascript">
  

function validatePassword(){

    var password = document.getElementById("password").value;
  var confirm_password = document.getElementById("confirm_password").value;

  if(password != confirm_password) {
    alert("Passwords Don't Match");
    return false;
  } else {
    return true;
  }
  
}

function ShowLoader(filename){

  var Confirm=confirm('Are you sure you want to Restore this Backup , thats mean the current patients data will be deleted and then restore this backup ?');

  if(Confirm == true){
    document.getElementById('loader').style.display='block';
    document.getElementById('filename').innerHTML=filename;

  }
  else {
    return false;
  }

}


function ShowBackupLoader(){
  document.getElementById('Backuploader').style.display='block';
}


</script>

<div class="container-fluid">
  <div class="row">
    <div class="col-md-3">
      <ul class="list-group text-right">
        <li class="list-group-item "><a href="viewpatient" >Setup</a></li>
      </ul>
    </div>
    <div class="col-md-7 lft-part">
      <div class="row">
          <?php if(session('status')): ?>
            <div id="alertmsg" class="alert alert-success">
                <?php echo e(session('status')); ?>

            </div>
            <?php endif; ?>

        <div class="col-md-12">
          <div class="row">

             <div class="col-md-6">
            <h3>Backup Patient Data</h3>
                      <div class="col-md-6">
                        <div class="form-group">
                         <a href="<?php echo e(url('BackupMyPatients')); ?>"><button type="submit" onClick="return ShowBackupLoader();" class="btn btn-primary btn-cus container-fluid btnmarg" name="btn-signup">Backup Data</button></a>
                        </div>

                        <br>
                             <div id="Backuploader">
                               <h4>New Backup in progress</h4>
                              <h5>this process may take more times according to database size , please be patient , thank you !</h5>
                              <div class="loader"></div>
                             </div>
                             
                      </div>

                              
            </div>

            <div class="col-md-6">
            <h3>Restore Patient Data</h3>
                        
                          <?php
                              $backupfiles = App\Http\Controllers\PatientController::getUserBackupfiles();
                           ?>

                            <?php $__currentLoopData = $backupfiles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $backupfile): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <p>
                                <span> <?php echo e($backupfile->link); ?> - <?php echo e($backupfile->Createdate); ?></span> 

                                <a href="<?php echo e(url('Import')); ?>/<?php echo e($backupfile->id); ?>" class="crossbtn"><span class="glyphicon glyphicon-repeat" title="Restore Backup" onClick="return ShowLoader('<?php echo e($backupfile->link); ?>-<?php echo e($backupfile->Createdate); ?>');"></span></a>

                                <a href="<?php echo e(url('')); ?>/backupfiles/<?php echo e($backupfile->link); ?>.sql" class="crossbtn"><span class="glyphicon glyphicon-download" title="Download Backup"></span></a>


                                <a href="<?php echo e(url('DeleteBackup')); ?>/<?php echo e($backupfile->id); ?>" class="crossbtn" onClick="return confirm('Are you sure you want to delete this item?');">
                                 <span class="glyphicon glyphicon-remove" title="Delete Backup"></span>
                                </a>



                                </p>
                             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                             <br>
                             <div id="loader">
                               <h4>Restore Backup in progress</h4><span id="filename"></span>
                              <h5>this process may take more times according to database size , please be patient , thank you !</h5>
                              <div class="loader"></div>
                             </div>
                             
                   
            </div>


          </div>
        </div>    

          

        <div class="col-md-12">
          <div class="row">
            
            <br><br>
          <hr>
                <div class="col-md-6">
                <div class="row">
                <div class="row chgpwdform">
                 
                 

            <form class="form-signin" id="register-form" method="post"  action="chgpwd">
              <h2 class="form-signin-heading">Change Password</h2>
                <div class="col-md-6">
                <div class="row">
                <div class="row">

                 <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                 <input type="hidden" id="user_id" name="user_id" value="<?php echo e(Auth::user()->id); ?>">

                 
                  <div class="col-md-12">
                    <div class="form-group">
                      <input type="password" class="form-control" placeholder="Current password" id="oldpwd" name="oldpwd" required  />
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-group">
                      <input type="password" class="form-control" placeholder="password" id="password" name="password" required  />
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-group">
                      <input type="password" class="form-control" placeholder="confirm password" id="confirm_password" name="confirm_password" required  />
                    </div>
                  </div>

                  <div class="col-md-12">
                        <div class="form-group">
                          <button type="submit" onclick="return validatePassword()" class="btn btn-primary btn-cus container-fluid btnmarg" name="btn-signup">Change Password</button>
                        </div>
                      </div>
                 
                </div>
              </div>
              </div>
            </form>
                 
                 
                </div>
              </div>  
              </div>
              

              <div class="col-md-6">
                <div class="row">
                <div class="row chgpwdform">
                 
                 

            <form class="form-signin" id="register-form" method="post"  action="DeleteAccount">
              <h2 class="form-signin-heading">Delete Account</h2>
                <div class="col-md-6">
                <div class="row">
                <div class="row">

                 <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                 <input type="hidden" id="user_id" name="user_id" value="<?php echo e(Auth::user()->id); ?>">


                  <div class="col-md-12">
                        <div class="form-group">
                          <button type="submit" onClick="return confirm('Are you sure you want to delete this item?');" class="btn btn-primary btn-cus container-fluid btnmarg" name="btn-signup">Delete Account</button>
                        </div>
                      </div>
                 
                </div>
              </div>
              </div>
            </form>
                 
                 
                </div>
              </div>  
              </div>


          </div>
        </div>
      </div>
    </div>
  </div>
</div>
 
<?php $__env->stopSection(); ?>




<?php echo $__env->make('app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>