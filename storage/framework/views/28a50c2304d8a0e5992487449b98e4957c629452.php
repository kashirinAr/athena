<?php $__env->startSection('title'); ?>
    <title>Telemed | Edit Patient</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('menu'); ?>
<li><a href="<?php echo e(url('home')); ?>" >HOME</a></li>
<li class="active"><a href="<?php echo e(url('viewpatient')); ?>">PATIENTS</a></li>
<li><a href="<?php echo e(url('callpatient')); ?>">CALL</a></li>
<li><a  href="<?php echo e(url('setup')); ?>">SETUP</a></li>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
<script>
  var loadFile = function(event) {
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
  };
</script>


<div class="container-fluid">
  <div class="row">
    <div class="col-md-3">
      <ul class="list-group text-right">
        <li class="list-group-item "><a href="<?php echo e(url('viewpatient')); ?>" >View Patients</a></li>
                <li class="list-group-item"><a href="<?php echo e(url('addpatientpage')); ?>" class="left-nav-active">Edit Patient</a></li>
        <!--<li class="list-group-item"><a href="">Edit Patient Data</a></li>-->
      </ul>
    </div>
    <div class="col-md-7 lft-part">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
                    <?php if(session('status')): ?>
                        <div id="alertmsg" class="alert alert-success">
                            <?php echo e(session('status')); ?>

                        </div>
                    <?php endif; ?>


            <form class="form-signin" id="register-form" method="post" enctype="multipart/form-data" action="<?php echo e(url('editpatient')); ?>">
              <h2 class="form-signin-heading">Edit Patient</h2>
                            <div class="col-md-12">
                <div class="row">
                <div class="row">

                 <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                 <input type="hidden" name="patientid" value="<?php echo e($patient->PatientProfileId); ?>">

                 <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">

                    <div class="col-md-6">
                      <div class="col-md-12">
                      <div class="form-group">
                         <label>Prefix</label>
                        <select  class="form-control" name="Prefix">
                                  <option <?php if($patient->Prefix ==""): ?> selected <?php endif; ?> value="">-- Select Prefix --</option>
                                  <option <?php if($patient->Prefix =="1st Lt"): ?> selected <?php endif; ?> value="1st Lt">1st Lt</option>
                                  <option <?php if($patient->Prefix =="Adm"): ?> selected <?php endif; ?>  value="Adm">Adm</option>
                                  <option <?php if($patient->Prefix =="Atty"): ?> selected <?php endif; ?>  value="Atty">Atty</option>
                                  <option <?php if($patient->Prefix =="Brother"): ?> selected <?php endif; ?>  value="Brother">Brother</option>
                                  <option <?php if($patient->Prefix =="Capt"): ?> selected <?php endif; ?>  value="Capt">Capt</option>
                                  <option <?php if($patient->Prefix =="Chief"): ?> selected <?php endif; ?>  value="Chief">Chief</option>
                                  <option <?php if($patient->Prefix =="Cmdr"): ?> selected <?php endif; ?>  value="Cmdr">Cmdr</option>
                                  <option <?php if($patient->Prefix =="Col"): ?> selected <?php endif; ?>  value="Col">Col</option>
                                  <option <?php if($patient->Prefix =="Dean"): ?> selected <?php endif; ?>  value="Dean">Dean</option>
                                  <option <?php if($patient->Prefix =="Dr"): ?> selected <?php endif; ?>  value="Dr">Dr</option>
                                  <option <?php if($patient->Prefix =="Elder"): ?> selected <?php endif; ?>  value="Elder">Elder</option>
                                  <option <?php if($patient->Prefix =="Father"): ?> selected <?php endif; ?>  value="Father">Father</option>
                                  <option  <?php if($patient->Prefix =="Gen"): ?> selected <?php endif; ?> value="Gen">Gen</option>
                                  <option <?php if($patient->Prefix =="Gov"): ?> selected <?php endif; ?>  value="Gov">Gov</option>
                                  <option <?php if($patient->Prefix =="Hon"): ?> selected <?php endif; ?>  value="Hon">Hon</option>
                                  <option <?php if($patient->Prefix =="Lt Col"): ?> selected <?php endif; ?>  value="Lt Col">Lt Col</option>
                                  <option <?php if($patient->Prefix =="Maj"): ?> selected <?php endif; ?>  value="Maj">Maj</option>
                                  <option  <?php if($patient->Prefix =="MSgt"): ?> selected <?php endif; ?> value="MSgt">MSgt</option>
                                  <option <?php if($patient->Prefix =="Mr"): ?> selected <?php endif; ?>  value="Mr">Mr</option>
                                  <option <?php if($patient->Prefix =="Mrs"): ?> selected <?php endif; ?>  value="Mrs">Mrs</option>
                                  <option <?php if($patient->Prefix =="Ms"): ?> selected <?php endif; ?>  value="Ms">Ms</option>
                                  <option <?php if($patient->Prefix =="Prince"): ?> selected <?php endif; ?>  value="Prince">Prince</option>
                                  <option <?php if($patient->Prefix =="Prof"): ?> selected <?php endif; ?>  value="Prof">Prof</option>
                                  <option <?php if($patient->Prefix =="Rabbi"): ?> selected <?php endif; ?>  value="Rabbi">Rabbi</option>
                                  <option <?php if($patient->Prefix =="Rev"): ?> selected <?php endif; ?>  value="Rev">Rev</option>
                                  <option <?php if($patient->Prefix =="Sister"): ?> selected <?php endif; ?>  value="Sister">Sister</option>
                                </select>
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="form-group">
                        <label>First Name *</label>
                        <input type="text" class="form-control" value="<?php echo e($patient->First); ?>" placeholder="First Name *" name="First"   required />
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="form-group">
                        <label>M-Name *</label>
                        <input type="text" class="form-control" value="<?php echo e($patient->Middle); ?>" placeholder="Middle Name"  name="Middle"    />
                      </div>
                    </div>

                    <div class="col-md-4">
                      <div class="form-group">
                        <label>Last Name *</label>
                        <input type="text" class="form-control" value="<?php echo e($patient->Last); ?>" placeholder="Last Name *"  name="Last"  required  />
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                         <label>Suffix</label>
                         <select  class="form-control" name="Suffix">
                                  <option <?php if($patient->Suffix =="Sister"): ?> selected <?php endif; ?>  value="">-- Select Suffix --</option>
                                  <option <?php if($patient->Suffix =="II"): ?> selected <?php endif; ?>  value="II">II</option>
                                  <option <?php if($patient->Suffix =="III"): ?> selected <?php endif; ?>  value="III">III</option>
                                  <option <?php if($patient->Suffix =="IV"): ?> selected <?php endif; ?>  value="IV">IV</option>
                                  <option <?php if($patient->Suffix =="CPA"): ?> selected <?php endif; ?>  value="CPA">CPA</option>
                                  <option <?php if($patient->Suffix =="DDS"): ?> selected <?php endif; ?>  value="DDS">DDS</option>
                                  <option <?php if($patient->Suffix =="Esq"): ?> selected <?php endif; ?>  value="Esq">Esq</option>
                                  <option <?php if($patient->Suffix =="JD"): ?> selected <?php endif; ?>  value="JD">JD</option>
                                  <option <?php if($patient->Suffix =="Jr"): ?> selected <?php endif; ?>  value="Jr">Jr</option>
                                  <option <?php if($patient->Suffix =="LLD"): ?> selected <?php endif; ?>  value="LLD">LLD</option>
                                  <option <?php if($patient->Suffix =="MD"): ?> selected <?php endif; ?>  value="MD">MD</option>
                                  <option <?php if($patient->Suffix =="PhD"): ?> selected <?php endif; ?>  value="PhD">PhD</option>
                                  <option <?php if($patient->Suffix =="Ret"): ?> selected <?php endif; ?>  value="Ret">Ret</option>
                                  <option <?php if($patient->Suffix =="RN"): ?> selected <?php endif; ?>  value="RN">RN</option>
                                  <option <?php if($patient->Suffix =="Sr"): ?> selected <?php endif; ?>  value="Sr">Sr</option>
                                  <option <?php if($patient->Suffix =="DO"): ?> selected <?php endif; ?>  value="DO">DO</option>
                                </select>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Nickname</label>
                        <input type="text" class="form-control" value="<?php echo e($patient->Nickname); ?>"  placeholder="Nickname" name="Nickname"    />
                      </div>
                    </div>

                       <div class="col-md-12">
                              <br>
                              <div class="form-group radio-label">
                                <label>Sex</label>            
                               <input class="custom-radio" type="radio" checked="checked" <?php if($patient->Sex =='Male'): ?>  checked="checked" <?php endif; ?> name="sex" id="3" value="Male">
                               <label for="3"> Male</label>
                               <input class="custom-radio" <?php if($patient->Sex =='Female'): ?>  checked="checked" <?php endif; ?> type="radio" name="sex" id="4" value="Female">
                               <label for="4"> Female</label>
                              </div>
                               <br>
                            </div>

                    

                  </div>

                  <div class="col-md-6">
                          
                              <div class="row">


                                <div class="col-md-12">
                                  <div class="form-group"> 
                                                          <img src="<?php echo e(url('')); ?>/<?php echo e($patient->Picture); ?>" id="output" alt="" class="pic-pp"> 
                                    
                                  </div>
                                </div>
                                <div class="col-md-12">
                                  <div class="form-group"> 
                                    <!--<input type="file" name="image" id="image">-->
                                    <label class="btn btn-primary btn-cus"> Upload Photo
                                      <input type="file"  onchange="loadFile(event)"  name="photo" id="image" style="display: none;">
                                    </label>
                                  </div>
                                </div>

   
                               
                          </div>
                  </div>
                </div>
              </div>
              </div>


               <div class="col-md-12">

                 <div class="col-md-12">
                      <div class="form-group">
                         <label>Address-1 *</label>
                        <input type="text" class="form-control" value="<?php echo e($patient->Address1); ?>"  placeholder="Address-1 *" name="Address1"  required  />
                      </div>
                    </div>

                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Address-2 *</label>
                        <input type="text" class="form-control" value="<?php echo e($patient->Address2); ?>"  placeholder="Address-2" name="Address2"    />
                      </div>
                    </div>


                    <div class="col-md-6">
                      <div class="form-group">
                         <label>City *</label>
                        <input type="text" class="form-control" value="<?php echo e($patient->City); ?>"  placeholder="City *" name="City"   required />
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                         <label>State *</label>
                        <input type="text" class="form-control" value="<?php echo e($patient->State); ?>"  placeholder="State *" name="State" required  />
                      </div>
                    </div>

                        <div class="col-md-2">
                          <div class="form-group">
                            <label>Zip *</label>
                            <input type="number" class="form-control"  value="<?php echo e($patient->Zip); ?>"  placeholder="Zip *" name="Zip"    required/>
                          </div>
                        </div>

                        <div class="col-md-4">
                          <div class="form-group">
                            <label>Country *</label>
                            <select class="form-control"  name="Country"  required> 
                              <option value="" selected="selected"  >-- Select Country -- </option> 
                              <option  <?php if($patient->Country =="USA"): ?> selected <?php endif; ?>  value="USA">United States</option> 
                            </select>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <label>County</label>
                            <input type="text" class="form-control"  value="<?php echo e($patient->County); ?>"  placeholder="County" name="County"    />
                          </div>
                        </div>

                            <div  class="col-md-4 ">
                              <div class="form-group">
                              <label>Address Type</label>
                                <input type="text" class="form-control"  value="<?php echo e($patient->AddressType); ?>"  placeholder="Address Type" name="AddressType"    />
                              </div>
                            </div>

                             <div class="col-md-6">
                              <div class="form-group">
                                <label>Phone1</label>
                                <input type="number" class="form-control"  value="<?php echo e($patient->Phone1); ?>"  placeholder="Phone-1 *" name="Phone1"   required />
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Phone1Type</label>
                                <select required class="form-control" name="Phone1Type">
                                  <option <?php if($patient->Phone1Type =="Home"): ?> selected <?php endif; ?>  value="Home">Home</option>
                                  <option <?php if($patient->Phone1Type =="Work"): ?> selected <?php endif; ?>  value="Work">Work</option>
                                  <option <?php if($patient->Phone1Type =="Mobile"): ?> selected <?php endif; ?>  value="Mobile">Mobile</option>
                                  <option <?php if($patient->Phone1Type =="Others"): ?> selected <?php endif; ?>  value="Others">Others</option>
                                </select>
                              </div>
                            </div>


                            <div class="col-md-12">
                    <div class="form-group">
                      <label>PhoneCarrier *</label>
                      <select class="form-control" id="PhoneCarrier" name="PhoneCarrier" required>
                        <option value="">-- Select Phone Carrier --</option>
                        <option value="gocbw.com" <?php if($patient->PhoneCarrier=="gocbw.com"): ?> selected <?php endif; ?>>Cincinnati & Ohio,Cincinnati Bell</option>
                        <option value="sms.alltelwireless.com" <?php if($patient->PhoneCarrier=="sms.alltelwireless.com"): ?> selected <?php endif; ?>>Alltel (Allied Wireless)</option>
                        <option value="mms.att.net" <?php if($patient->PhoneCarrier=="mms.att.net"): ?> selected <?php endif; ?>>AT&T Mobility (formerly Cingular)</option>
                        <option value="myboostmobile.com" <?php if($patient->PhoneCarrier=="myboostmobile.com"): ?> selected <?php endif; ?>>Boost Mobile</option>
                        <option value="mms.gocbw.com" <?php if($patient->PhoneCarrier=="mms.gocbw.com"): ?> selected <?php endif; ?>>Cincinnati Bell</option>
                        <option value="mms.cricketwireless.net" <?php if($patient->PhoneCarrier=="mms.cricketwireless.net"): ?> selected <?php endif; ?>>Cricke</option>
                        <option value="mymetropcs.com" <?php if($patient->PhoneCarrier=="mymetropcs.com"): ?> selected <?php endif; ?>>MetroPCS</option>
                        <option value="vtext.com" <?php if($patient->PhoneCarrier=="vtext.com"): ?> selected <?php endif; ?>>Page Plus Cellular(Verizon MVNO)</option>
                        <option value="smtext.com" <?php if($patient->PhoneCarrier=="smtext.com"): ?> selected <?php endif; ?>>Simple Mobile</option>
                        <option value="mms.att.net" <?php if($patient->PhoneCarrier=="mms.att.net"): ?> selected <?php endif; ?>>Straight Talk</option>
                        <option value="pm.sprint.com" <?php if($patient->PhoneCarrier=="pm.sprint.com"): ?> selected <?php endif; ?>>Sprint</option>
                        <option value="tmomail.net" <?php if($patient->PhoneCarrier=="tmomail.net"): ?> selected <?php endif; ?>>T-Mobile</option>
                        <option value="mms.uscc.net" <?php if($patient->PhoneCarrier=="mms.uscc.net"): ?> selected <?php endif; ?>>US Cellular</option>
                        <option value="vzwpix.com" <?php if($patient->PhoneCarrier=="vzwpix.com"): ?> selected <?php endif; ?>>Verizon Wireless</option>
                        <option value="vmpix.com" <?php if($patient->PhoneCarrier=="vmpix.com"): ?> selected <?php endif; ?>>Virgin Mobile</option>
                        <option value="mmst5.tracfone.com" <?php if($patient->PhoneCarrier=="mmst5.tracfone.com"): ?> selected <?php endif; ?>>Puerto Rico, and the US Virgin Islands,TracFone (prepaid)</option>
                      </select>
                    </div>
                  </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                 <label>Phone2</label>
                                <input type="number" class="form-control"  value="<?php echo e($patient->Phone2); ?>"  placeholder="Phone-2" name="Phone2"    />
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                 <label>Phone2Type</label>
                                 <select class="form-control" name="Phone1Type">
                                  <option <?php if($patient->Phone2Type ==""): ?> selected <?php endif; ?>  value="">-- Select Phone 2 Type --</option>
                                  <option <?php if($patient->Phone2Type =="Home"): ?> selected <?php endif; ?>  value="Home">Home</option>
                                  <option <?php if($patient->Phone2Type =="Work"): ?> selected <?php endif; ?>  value="Work">Work</option>
                                  <option <?php if($patient->Phone2Type =="Mobile"): ?> selected <?php endif; ?>  value="Mobile">Mobile</option>
                                  <option <?php if($patient->Phone2Type =="Others"): ?> selected <?php endif; ?>  value="Others">Others</option>
                                </select>
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                 <label>Phone3</label>

                                <input type="number" class="form-control"  value="<?php echo e($patient->Phone3); ?>"  placeholder="Phone-3" name="Phone3"    />
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                 <label>Phone3Type</label>

                                <select class="form-control" name="Phone1Type">
                                  <option <?php if($patient->Phone3Type ==""): ?> selected <?php endif; ?>  value="">-- Select Phone 2 Type --</option>
                                  <option <?php if($patient->Phone3Type =="Home"): ?> selected <?php endif; ?>  value="Home">Home</option>
                                  <option <?php if($patient->Phone3Type =="Work"): ?> selected <?php endif; ?>  value="Work">Work</option>
                                  <option <?php if($patient->Phone3Type =="Mobile"): ?> selected <?php endif; ?>  value="Mobile">Mobile</option>
                                  <option <?php if($patient->Phone3Type =="Others"): ?> selected <?php endif; ?>  value="Others">Others</option>
                                </select>
                              </div>
                            </div>

                            <div class="col-md-12">
                              <div class="form-group">
                                 <label>EMailAddress</label>

                                <input type="text" class="form-control"  value="<?php echo e($patient->EMailAddress); ?>"  placeholder="Email Address" name="EMailAddress"    />
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                 <label>AlternateAddress1</label>

                                <input type="text" class="form-control"  value="<?php echo e($patient->AlternateAddress1); ?>"  placeholder="Alternate Address-1" name="AlternateAddress1"    />
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                 <label>AlternateAddress2</label>

                                <input type="text" class="form-control"  value="<?php echo e($patient->AlternateAddress2); ?>"  placeholder="Alternate Address-2" name="AlternateAddress2"    />
                              </div>
                            </div>

                            <div class="col-md-4">
                              <div class="form-group">
                                 <label>AlternateCity</label>

                                <input type="text" class="form-control"  value="<?php echo e($patient->AlternateCity); ?>"  placeholder="Alternate City" name="AlternateCity"    />
                              </div>
                            </div>

                            <div class="col-md-4">
                              <div class="form-group">
                                 <label>AlternateState</label>

                                <input type="text" class="form-control"  value="<?php echo e($patient->AlternateState); ?>"  placeholder="Alternate State" name="AlternateState"    />
                              </div>
                            </div>

                            <div class="col-md-4">
                              <div class="form-group">
                                 <label>AlternateZip</label>

                                <input type="number" class="form-control"   value="<?php echo e($patient->AlternateZip); ?>"  placeholder="Alternate Zip" name="AlternateZip"    />
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                 <label>AlternateCounty</label>

                                <input type="text" class="form-control"  value="<?php echo e($patient->AlternateCounty); ?>"   placeholder="Alternate County" name="AlternateCounty"    />
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                 <label>AlternateCountry</label>
                                <select class="form-control" name="AlternateCountry" name="AlternateCountry" > 
                              <option value="" selected="selected"  >-- Select Country -- </option> 
                              <option value="USA"  <?php if($patient->AlternateCountry=="USA"): ?> selected <?php endif; ?>  >United States</option> 
                            </select>
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                 <label>AlternateAddressType</label>

                                <input type="text" class="form-control"   value="<?php echo e($patient->AlternateAddressType); ?>"  placeholder="Alternate Address Type" name="AlternateAddressType"    />
                              </div>
                            </div>


                            <div class="col-md-6">
                              <div class="form-group">
                                 <label>SchoolName</label>

                                <input type="text" class="form-control"   value="<?php echo e($patient->SchoolName); ?>"  placeholder="School Name" name="SchoolName"    />
                              </div>
                            </div>

                            <div class="col-md-12 ">
                              <div class="form-group">
                                 <label>SSN</label>

                                <input type="text" class="form-control"   value="<?php echo e($patient->SSN); ?>"  placeholder="SSN" name="SSN"    />
                              </div>
                            </div>


                            <div class="col-md-6 ">
                              <div class="form-group">
                                 <label>Birthdate</label>
                                <?php
                                  $dt1 = new DateTime($patient->Birthdate);
                                  $Birthdate = $dt1->format('Y-m-d');

                                  $dt2 = new DateTime($patient->DeathDate);
                                  $DeathDate = $dt2->format('Y-m-d');

                                 ?>
                                <input type="date" class="form-control"   value="<?php echo e($Birthdate); ?>"  name="Birthdate"   id="Birthdate" />
                              </div>
                              
                            </div>
                            
                            <div class="col-md-6">
                              <div class="form-group">
                                 <label>DeathDate</label>

                                <input type="date" class="form-control"   value="<?php echo e($DeathDate); ?>"  placeholder="DeathDate" name="DeathDate"  id="DeathDate"    />
                              </div>
                            </div>



                            <div class="col-md-4">
                              <div class="form-group">
                                 <label>Referred By PatientId</label>

                                <input type="number" class="form-control"   value="<?php echo e($patient->ReferredByPatientId); ?>"  placeholder="Referred By PatientId" name="ReferredByPatientId"    />
                              </div>
                            </div>

                            <div class="col-md-4">
                              <div class="form-group">
                                 <label>Patient Same As Guarantor</label>

                                <select class="form-control" name="PatientSameAsGuarantor">
                                  <option <?php if($patient->PatientSameAsGuarantor ==""): ?> selected <?php endif; ?>  value="">-- Patient Same As Guarantor --</option>
                                  <option <?php if($patient->PatientSameAsGuarantor =="1"): ?> selected <?php endif; ?>  value="1">Yes</option>
                                  <option <?php if($patient->PatientSameAsGuarantor !="1"): ?> selected <?php endif; ?>  value="0">No</option>
                                </select>
                              </div>
                            </div>

                            <div class="col-md-4">
                              <div class="form-group">
                                 <label>Medical Record Number</label>

                                <input type="text" class="form-control"  value="<?php echo e($patient->MedicalRecordNumber); ?>" placeholder="Medical Record Number" name="MedicalRecordNumber"    />
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                 <label> Visit Document No. </label>
                                <input type="number" class="form-control"   value="<?php echo e($patient->visdocnum); ?>" placeholder="Visit Document No." name="visdocnum"    />
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <label>SSID</label>
                                <input type="text" class="form-control"   value="<?php echo e($patient->SSDID); ?>" placeholder="SSDID" name="SSDID"  />
                              </div>
                            </div>

                            <div class="col-md-12">
                            <div class="form-group">
                                <label>Profile Notes</label>

                              <textarea class="form-control"   value="" placeholder="Profile Notes" id="ProfileNotes" name="ProfileNotes"   ><?php echo e($patient->ProfileNotes); ?></textarea>
                            </div>
                            </div>


                            <div class="col-md-12">
                            <div class="form-group">
                                <label>Alert Notes </label>

                            <textarea class="form-control"   value="" placeholder="Alert Notes" id="AlertNotes" name="AlertNotes"  ><?php echo e($patient->AlertNotes); ?></textarea>
                            </div>
                            </div>


                            <div class="col-md-12">
                            <div class="form-group">
                                <label>Appointment Notes </label>

                              <textarea class="form-control"   value="" placeholder="Appointment Notes" id="AppointmentNotes" name="AppointmentNotes"   ><?php echo e($patient->AppointmentNotes); ?></textarea>
                            </div>
                            </div>


                            <div class="col-md-12">
                            <div class="form-group">
                              <label>Billing Notes</label>
                              <textarea class="form-control"  value=""  placeholder="Billing Notes" id="BillingNotes" name="BillingNotes"   ><?php echo e($patient->BillingNotes); ?></textarea>
                            </div>
                            </div>


                            <div class="col-md-12">
                            <div class="form-group">
                              <label>Reg Note</label>

                              <textarea class="form-control"  value=""  placeholder="Reg Note" id="RegNote" name="RegNote"   ><?php echo e($patient->RegNote); ?></textarea>
                            </div>
                            </div>

                             <div class="col-md-12">
                                  <div class="form-group">
                                    <button type="submit" class="btn btn-default btn-green" name="btn-signup">Update Patient Data </button>
                                  </div>
                                </div>


                      </div>

                 
                    
                      




                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php $__env->stopSection(); ?>




<?php echo $__env->make('app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>