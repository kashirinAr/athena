<?php $__env->startSection('title'); ?>
    <title>Telemed | Add Patient</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('menu'); ?>
<li><a href="  <?php echo e(url('home')); ?>" >HOME</a></li>
<li class="active"><a href="<?php echo e(url('viewpatient')); ?> ">PATIENTS</a></li>
<li><a href="<?php echo e(url('callpatient')); ?> ">CALL</a></li>
<li><a  href="<?php echo e(url('setup')); ?>">SETUP</a></li>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>


<div class="container-fluid"> 
  <div class="row">
    <div class="col-md-3">
      <ul class="list-group text-right">
        <li class="list-group-item "><a href="<?php echo e(url('viewpatient')); ?> " >View / Edit Patients</a></li>
        <li class="list-group-item"><a href="<?php echo e(url('addpatientpage')); ?>" >Add Patient</a></li>
        <!--<li class="list-group-item" class="left-nav-active"><a href="">Edit Patient Data</a></li>-->
      </ul>
    </div>
    <div class="col-md-7 lft-part">
      <div class="row">
        <div class="col-md-12">
           
            <h2 class="form-signin-heading"><span class="text text-info small">Pending Telehealth<span> <?php echo e($patient->First); ?> <?php echo e($patient->Middle); ?> <?php echo e($patient->Last); ?></h2>
     <hr />
      <?php if(session('status')): ?>
                      <div id="alertmsg" class="alert alert-success">
                          <?php echo e(session('status')); ?>

                      </div>
      <?php endif; ?>
          <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">

          <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="update-box">
                <h3>Setup Patient</h3>
                <br>
                <p>
                  Complete patient infomation to set them up for virtual calls.
                </p>
                <br>
                <div id='complete-setup' class="update-button">
                  Complete Patient Setup
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="update-box">
                <h3>Get Patient Approval</h3>
                <br>
                <p>
                  Connect with the patient to timestamp approval for the
                  telemedicine call.
                </p>
                <br>
                <div id='confirm-approval' class="update-button">
                  Confirm Patient Approval
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="update-box">
                <h3>Create Appointments</h3>
                <br>
                <p>
                  Create an appointment date to remind the patient of their
                  upcoming Telehealth appointment.
                </p>
                <br>
                <div id='set-app' class="update-button">
                  Set App with (<?php echo e($patient->First); ?>)
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="update-box">
                <h3>Have TeleVideo meeting and take notes</h3>
                <br>
                <p>
                  Start Telehealth call.
                </p>
                <br>
                <div id='start-call' class="update-button">
                  Start Call with (<?php echo e($patient->First); ?>)
                </div>
              </div>
            </div>

            </div>

          </div>
        

     <hr />

      
                        
                                        
      </div>
    </div>

    
    <div class="modal fade" id="myModal-box1" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Setup Patient</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-lg-4">
                <h4 class="text text-info">FullName</h4>
              </div>
              <div class="col-lg-8">
                <h4><?php echo e($patient->First); ?> <?php echo e($patient->Middle); ?> <?php echo e($patient->Last); ?></h4>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-4">
                <h4 class="text text-info">EMailAddress</h4>
              </div>
              <div class="col-lg-8">
                <h4><?php echo e($patient->EMailAddress); ?></h4>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-4">
                <h4 class="text text-info">PhoneCarrier</h4>
              </div>
              <div class="col-lg-8">
                <h4><?php echo e($patient->PhoneCarrier); ?></h4>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-4">
                <h4 class="text text-info">PhoneNumbers</h4>
              </div>
              <div class="col-lg-8">
                <h4><?php echo e($patient->Phone1); ?> <?php echo e($patient->Phone2); ?> <?php echo e($patient->Phone3); ?></h4>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <div class="progress hidden">
              <div class="progress-bar progress-bar-striped progress-bar-animated active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
            </div>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-success" id="confirm-box1">Confirm</button>
          </div>
        </div>
      </div>
    </div>
    
    <div class="modal fade" id="myModal-box2" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Get Patient  Approval</h4>
          </div>
          <div class="modal-body">
            <div class="myquestion">
              <p>my question:</p>
              <h4 class="text text-info">1. Are you giving consent to discuss your medical via telemedicine?</h4>
            </div>
            <div class="answer form-control">
              <span>patient answer:</span>
              <select id='answer1' name="answer1">
                <option name="answer1" value="yes">Yes</option>
                <option name="answer1" value="no">No</option>
              </select>
            </div>
            
            <br>

            <div class="myquestion">
              <p>my question:</p>
              <h4 class="text text-info">2. What is the way they wish to have the appointment, phone, or computer?</h4>
            </div>
            <div class="answer form-control">
              <span>patient answer:</span>
              <select id='answer2' name="answer2">
                <option name="answer2" value="appointment">Appointment</option>
                <option name="answer2" value="phone">Phone</option>
                <option name="answer2" value="computer">Computer</option>
              </select>
            </div>

          </div>
          <div class="modal-footer">
            <div class="progress hidden">
              <div class="progress-bar progress-bar-striped progress-bar-animated active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
            </div>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-success" id="confirm-box2">Confirm</button>
          </div>
        </div>
      </div>
    </div>
    
    <div class="modal fade" id="myModal-box3" role="dialog">
      <div class="modal-dialog">
      
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Create Appointments</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-lg-4">
                <h4 class="text text-info">Full Name</h4>
              </div>
              <div class="col-lg-8">
                <h4 id="fullname"><?php echo e($patient->First); ?> <?php echo e($patient->Middle); ?> <?php echo e($patient->Last); ?></h4>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-4">
                <h4 class="text text-info">Email</h4>
              </div>
              <div class="col-lg-8">
                <h4 id="email"><?php echo e($patient->EMailAddress); ?></h4>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-4">
                <h4 class="text text-info">Phone Numbers</h4>
              </div>
              <div class="col-lg-8">
                <h4 id="phone"><?php echo e($patient->Phone1); ?> <?php echo e($patient->Phone2); ?> <?php echo e($patient->Phone3); ?></h4>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-4">
                <h4 class="text text-info">DateTime</h4>
              </div>
              <div class="col-lg-8" style="padding-top: 10px">
                  <input type="datetime-local" id="datetime" name="datetime">
              </div>
            </div>
            <div class="row">
              <div class="col-lg-4">
                <h4 class="text text-info">Message</h4>
              </div>
              <div class="col-lg-8" style="padding-top: 10px">
                <textarea id="message" style="width: 100%; height:100px; resize: none;"></textarea>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <div class="progress hidden">
              <div class="progress-bar progress-bar-striped progress-bar-animated active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
            </div>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-success" id="confirm-box3">Confirm</button>
          </div>
        </div>
      </div>
    </div>

                     


  </div>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>
<script type="text/javascript">

///init
var completeStatus = <?php echo e($patient->completeStatus); ?>


<?php if(count($approval)===1): ?>
  $("#answer1").val('<?php echo e($approval[0]->answer1); ?>');
  $("#answer2").val('<?php echo e($approval[0]->answer2); ?>');
<?php endif; ?>

if(completeStatus>0){
  $('#complete-setup').parent().addClass('active')
}
if(completeStatus>1){
  $('#confirm-approval').parent().addClass('active')
}
if(completeStatus>2){
  $('#set-app').parent().addClass('active')
}
if(completeStatus>3){
  $('#start-call').parent().addClass('active')
}

////////////

///box1
  $("#complete-setup").click(function(){
    var isActive = false;
    if($(this).parent().attr('class') == "update-box active"){
      isActive = true;
    }
    if(!isActive){
      
      $("#myModal-box1").modal("show");
      
    }
  });

  $("#confirm-box1").click(function(){
    if($('.progress').attr('class') == "progress")return;
    <?php if($patient->EMailAddress == "" || $patient->PhoneCarrier == ""): ?>
        alert("Please set EMailAddress or PhoneCarrier!");
        window.open("<?php echo e(url('editpatientpage/'.$patient->PatientProfileId)); ?>", '_self');
        return;
      <?php endif; ?>
    $('.progress').removeClass('hidden');
    $.ajax({
      type:'POST',
      url:'<?php echo e(url('ajaxUpdateSetupPatient')); ?>',
      data:{'patientid': <?php echo e($patient->PatientProfileId); ?>, '_token': $('input[name=_token]').val()},
      success:function(data){
        $('.progress').addClass('hidden');
        $("#myModal-box1").modal("hide");
        $('#complete-setup').parent().addClass('active');
        completeStatus = 1;
      }
    });
  });

  
///box2
$("#confirm-approval").click(function(){
    var isActive = false;
    if($(this).parent().attr('class') == "update-box active"){
      isActive = true;
    }
    if(!isActive){
      if(completeStatus == 1){
        $("#myModal-box2").modal("show");
      }else{
        alert("Please setup patient!");
      }
    }
  });

  $("#confirm-box2").click(function(){
    if($('.progress').attr('class') == "progress")return;
    $('.progress').removeClass('hidden');
    $.ajax({
      type:'POST',
      url:'<?php echo e(url('ajaxUpdateGetApproval')); ?>',
      data:{'patientid': <?php echo e($patient->PatientProfileId); ?>,'answer1':$("#answer1").val(), 'answer2':$("#answer2").val(), '_token': $('input[name=_token]').val()},
      success:function(data){
        $('.progress').addClass('hidden');
        $("#myModal-box2").modal("hide");
        if(data=="yes"){
          $('#confirm-approval').parent().addClass('active');
          completeStatus = 2;
        }
      }
    });
  });

  
///box3
$("#set-app").click(function(){
    var isActive = false;
    if($(this).parent().attr('class') == "update-box active"){
      isActive = true;
    }
    if(!isActive){
      if(completeStatus == 2){
        $("#myModal-box3").modal("show");
      }else{
        alert("Please get patient approval!");
      }
    }
  });

  $("#confirm-box3").click(function(){
    if($('.progress').attr('class') == "progress")return;
    $('.progress').removeClass('hidden');
    $.ajax({
      type:'POST',
      url:'<?php echo e(url('ajaxUpdateCreateAppointment')); ?>',
      data:{
        'patientid': <?php echo e($patient->PatientProfileId); ?>,
        'name': $('#fullname').text(),
        'email':$('#email').text(),
        'phone':$('#phone').text(),
        'datetime':$('#datetime').val(),
        'message':$('#message').val(), '_token': $('input[name=_token]').val()},
      success:function(data){
        $('.progress').addClass('hidden');
        $("#myModal-box3").modal("hide");
        $('#set-app').parent().addClass('active');
        completeStatus = 3;
      }
    });
  });

  
///box4
$("#start-call").click(function(){
    var isActive = false;
    if($(this).parent().attr('class') == "update-box active"){
      isActive = true;
    }
    if(!isActive){
      if(completeStatus == 3){
        $.ajax({
          type:'POST',
          url:'<?php echo e(url('ajaxUpdateHaveMeeting')); ?>',
          data:{'patientid': <?php echo e($patient->PatientProfileId); ?>, '_token': $('input[name=_token]').val()},
          success:function(data){
            $('#start-call').parent().addClass('active');
            completeStatus = 4;
          }
        });
        window.open("<?php echo e(url('callpatient')); ?>", '_self');
      }else{
        alert("Please create appointments!");
      }
    }
  });


</script>
<?php $__env->stopSection(); ?>




<?php echo $__env->make('app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>